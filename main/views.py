from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.views import generic
from django.template import RequestContext
import django.contrib.auth as auth
from django.contrib.auth.decorators import login_required
from forms import *
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.decorators.debug import sensitive_variables, sensitive_post_parameters
from django.forms.util import ErrorList
from api import *
import paramiko
from django.views.decorators.cache import cache_control

from models import *

def wrap_error(text, form):
    errors = form._errors.setdefault(forms.forms.NON_FIELD_ERRORS, ErrorList())
    errors.append(text)

def getConfigFile(router):
    r = VyattaRouter(router.ip, router.user, router.password)

    try:
        r.connect()
        r.save_config()
        r.close_connection()
    except StandardError:
        raise

    plik = router.name
    plik += ".cfg"

    try:
        r.dump_config(plik)
    except StandardError:
        raise

    return plik

def put_config(router, plik):
    r = VyattaRouter(router.ip, router.user, router.password)

    try:
        r.put_config(plik)
    except StandardError:
        raise

    try:
        r.connect()
        r.load_config()
        r.close_connection()
    except StandardError:
        raise

def uploadConfig(request):
    # Handle file upload
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            all = ConfigFile.objects.all()
            for a in all:
                a.delete()
            newdoc = ConfigFile(file = request.FILES['file'], title=request.POST['title'])
            newdoc.save()

            routers = Routers.objects.filter(selected=True)
            path = os.path.basename(newdoc.file.url)
            for router in routers:
                put_config(router, path)

            # Redirect to the document list after POST
            return HttpResponseRedirect('/upload_config')
    else:
        form = UploadFileForm() # A empty, unbound form

    selected_list = Routers.objects.filter(selected=1)

    # Render list page with the documents and the form
    return render_to_response(
        'main/upload_config.html',
        {'form': form, 'selected_list': selected_list},
        context_instance=RequestContext(request)
    )

def index(request):
    #menu glowne
    if request.user.is_authenticated():
        return render_to_response('main/index.html', RequestContext(request))
    else:
        return HttpResponseRedirect("/login/")

def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = auth.authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user is not None:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                auth.login(request, user)
                return HttpResponseRedirect('/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your guiatta account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Zle dane logowania")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('main/login.html', {}, context)

@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    auth.logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/')

def refresh_router_configs(request):
    routers = Routers.objects.all()
    for router in routers:
        try:
            plik = getConfigFile(router)
        except StandardError:
            return HttpResponse("Blad przy pobieraniu configow")

        interfaces = Interfaces.objects.filter(router_id=router)
        bridges = Bridge.objects.filter(router_id=router)

        for interface in interfaces:
            interface.delete()

        for bridge in bridges:
            bridge.delete()

        try:
            filepath = BASE_DIR
            filepath += "/config_files"
            filepath = os.path.join(filepath, plik)
            interfaces = find_ethernet_interfaces(filepath)
            bridges = find_bridges(filepath)
        except StandardError:
            return HttpResponse("Blad przy pobieraniu interfejsow z configow")

        for interface in interfaces:
            i = Interfaces(router_id=router,
                            name=interface['name'],
                            hw_id=interface['mac_address'],
                            ip=interface['ip'],
                            description=interface['description'])

            try:
                i.save()

            except StandardError:
                return HttpResponse("Blad przy zapisywaniu interfejsow do bazy")

        for bridge in bridges:
            b = Bridge(router_id=router,
                       name=bridge['name'],
                       ip=bridge['ip'],
                       stp_enabled=bridge['stp_enabled'],
                       hw_id=bridge['mac'])

            try:
                b.save()

            except StandardError:
                return HttpResponse("Blad przy zapisywaniu bridgy do bazy")

        #return super(CreateRoutersView, self).form_valid(form)
        return HttpResponseRedirect("/routers/")


class InterfacesView(generic.ListView):
    model = Interfaces
    template_name = 'main/interfaces.html'

    def get_context_data(self, **kwargs):
        context = super(InterfacesView, self).get_context_data(**kwargs)
        context['selected_list'] = Routers.objects.filter(selected=1)
        context['interfaces_list'] = Interfaces.objects.order_by('id')
        return context

class CreateInterfacesView(CreateView):
    model = Interfaces
    template_name_suffix = '_create_form'
    success_url = "/interfaces/"
    form_class = InterfacesForm

    def dispatch(self, *args, **kwargs):
        return super(CreateInterfacesView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        router = Routers.objects.get(id=form.instance.router_id.pk)
        i = Interfaces(router_id=router,
                       name=form.instance.name,
                       hw_id=form.instance.hw_id,
                       ip=form.instance.ip,
                       description=form.instance.description)

        i.save()

        update_interface(router, i)
        return HttpResponseRedirect("/interfaces/")

class UpdateInterfacesView(UpdateView):
    model = Interfaces
    template_name_suffix = '_update_form'
    success_url = "/interfaces/"
    form_class = InterfacesForm

    def dispatch(self, *args, **kwargs):
        return super(UpdateInterfacesView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        router = Routers.objects.get(id=self.get_object().router_id.pk)

        try:
            super(UpdateInterfacesView, self).form_valid(form)
        except Exception:
            return HttpResponseRedirect("/interfaces/")

        interface = Interfaces.objects.get(id=self.get_object().pk)
        update_interface(router, interface)
        return HttpResponseRedirect("/interfaces/")

class DeleteInterfacesView(DeleteView):
    model = Interfaces
    template_name_suffix = '_delete_form'
    success_url = "/interfaces/"

    def dispatch(self, *args, **kwargs):
        return super(DeleteInterfacesView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        router = Routers.objects.get(id=self.get_object().router_id.pk)
        interface = Interfaces.objects.get(id=self.get_object().pk)
        try:
            super(DeleteInterfacesView, self).delete(request, args, kwargs)
        except Exception:
            return HttpResponseRedirect("/interfaces/")
        delete_inerface(router, interface)
        return HttpResponseRedirect("/interfaces/")

class BridgeView(generic.ListView):
    model = Bridge
    template_name = "main/bridges.html"

    def get_context_data(self, **kwargs):
        context = super(BridgeView, self).get_context_data(**kwargs)
        context['selected_list'] = Routers.objects.filter(selected=1)
        context['bridges_list'] = Bridge.objects.order_by('id')
        return context

class CreateBridgeView(CreateView):
    model = Bridge
    template_name_suffix = '_create_form'
    success_url = "/bridges/"
    form_class = BridgeForm

    def dispatch(self, *args, **kwargs):
        return super(CreateBridgeView, self).dispatch(*args, **kwargs)

class UpdateBridgeView(UpdateView):
    model = Bridge
    template_name_suffix = '_update_form'
    success_url = "/bridges/"
    form_class = BridgeForm

    def dispatch(self, *args, **kwargs):
        return super(UpdateBridgeView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        router = Routers.objects.get(id=self.get_object().router_id.pk)

        try:
            super(UpdateBridgeView, self).form_valid(form)
        except Exception:
            return HttpResponseRedirect("/bridge/")

        bridge = Bridge.objects.get(id=self.get_object().pk)
        update_bridge(router, bridge)
        return HttpResponseRedirect("/bridges/")

class DeleteBridgeView(DeleteView):
    model = Bridge
    template_name_suffix = '_delete_form'
    success_url = "/bridges/"

    def dispatch(self, *args, **kwargs):
        return super(DeleteBridgeView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        router = Routers.objects.get(id=self.get_object().router_id.pk)
        bridge = Bridge.objects.get(id=self.get_object().pk)
        try:
            super(DeleteBridgeView, self).delete(request, args, kwargs)
        except Exception:
            return HttpResponseRedirect("/bridges/")
        delete_bridge(router, bridge)
        return HttpResponseRedirect("/bridges/")

class RoutersView(generic.ListView):
    model = Routers
    template_name = "main/routers.html"

    def get_context_data(self, **kwargs):
        context = super(RoutersView, self).get_context_data(**kwargs)
        context['routers_list'] = Routers.objects.order_by('id')
        return context

class CreateRoutersView(CreateView):
    model = Routers
    template_name_suffix = '_create_form'
    success_url = "/routers/"
    form_class = RouterForm

    def dispatch(self, *args, **kwargs):
        return super(CreateRoutersView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        try:
            plik = getConfigFile(form.instance)
        except StandardError:
            wrap_error(u"Could NOT connect to that router and get config file.", form)
            return super(CreateRoutersView, self).form_invalid(form)

        try:
            filepath = BASE_DIR
            filepath += "/config_files"
            filepath = os.path.join(filepath, plik)
            interfaces = find_ethernet_interfaces(filepath)
            bridges = find_bridges(filepath)
        except StandardError:
            wrap_error(u"Could NOT parse router config file.", form)
            return super(CreateRoutersView, self).form_invalid(form)

        c = Routers(name=form.instance.name,
                            ip=form.instance.ip,
                            user=form.instance.user,
                            password=form.instance.password,
                            selected=form.instance.selected)

        c.save()

        for interface in interfaces:
            i = Interfaces(router_id=c,
                           name=interface['name'],
                           hw_id=interface['mac_address'],
                           ip=interface['ip'],
                           description=interface['description'])

            try:
                i.save()

            except StandardError:
                wrap_error(u"Could NOT save interface object.", form)
                return super(CreateRoutersView, self).form_invalid(form)

        for bridge in bridges:
            b = Bridge(router_id=c,
                       name=bridge['name'],
                       ip=bridge['ip'],
                       hw_id=bridge['mac'],
                       description=bridge['description'])

            try:
                b.save()

            except StandardError:
                wrap_error("Blad przy zapisywaniu bridgy do bazy", form)
                return super(CreateRoutersView, self).form_invalid(form)

        #return super(CreateRoutersView, self).form_valid(form)
        return HttpResponseRedirect("/routers/")

class UpdateRoutersView(UpdateView):
    model = Routers
    template_name_suffix = '_update_form'
    success_url = "/routers/"
    form_class = RouterForm

    def dispatch(self, *args, **kwargs):
        return super(UpdateRoutersView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        router_old = Routers.objects.get(id=self.get_object().pk)
        try:
            super(UpdateRoutersView, self).form_valid(form)
        except Exception:
            return HttpResponseRedirect("/interfaces/")

        try:
            plik = getConfigFile(form.instance)
        except StandardError:
            wrap_error(u"Could NOT connect to that router and get config file.", form)
            return super(UpdateRoutersView, self).form_invalid(form)

        router = Routers.objects.get(id=self.get_object().pk)
        if router_old.ip != router.ip:
            interfaces = Interfaces.objects.filter(router_id=router)

            for interface in interfaces:
                interface.delete()

            try:
                filepath = BASE_DIR
                filepath += "/config_files"
                filepath = os.path.join(filepath, plik)
                interfaces = find_ethernet_interfaces(filepath)
                bridges = find_bridges(filepath)
            except StandardError:
                wrap_error(u"Could NOT parse router config file.", form)
                return super(UpdateRoutersView, self).form_invalid(form)

            for interface in interfaces:
                i = Interfaces(router_id=router,
                               name=interface['name'],
                               hw_id=interface['mac_address'],
                               ip=interface['ip'],
                               description=interface['description'])

                try:
                    i.save()

                except StandardError:
                    wrap_error(u"Could NOT save interface object.", form)
                    return super(UpdateRoutersView, self).form_invalid(form)

            for bridge in bridges:
                b = Bridge(router_id=router,
                           name=bridge['name'],
                           ip=bridge['ip'],
                           hw_id=bridge['mac'],
                           description=bridge['description'])

                try:
                    b.save()

                except StandardError:
                    wrap_error("Blad przy zapisywaniu bridgy do bazy", form)
                    return super(UpdateRoutersView, self).form_invalid(form)
        #return super(CreateRoutersView, self).form_valid(form)
        return HttpResponseRedirect("/routers/")

class DeleteRoutersView(DeleteView):
    model = Routers
    template_name_suffix = '_delete_form'
    success_url = "/routers/"

    def dispatch(self, *args, **kwargs):
        return super(DeleteRoutersView, self).dispatch(*args, **kwargs)

def testview(request):
    router = VyattaRouter('192.168.0.11', 'vyatta', '1q2w3e')
    router.connect()
    #router.save_config()
    #router.dump_config("config.cfg")
    #router.put_config()
    #router.load_config()
    router.close_connection()
    if request.user.is_authenticated():
        return render_to_response('main/config_file_test.html', {}, RequestContext(request))
    else:
        return HttpResponseRedirect("/login/")