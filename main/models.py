from django.db import models
import django.contrib.auth.hashers

# Create your models here.

class Routers(models.Model):
    name = models.CharField(max_length=200)
    ip = models.IPAddressField()
    user = models.CharField(max_length=100)
    password = models.CharField(max_length=50)
    selected = models.BooleanField(default=0)

    def __unicode__(self):
        return self.name

class Bridge(models.Model):
    router_id = models.ForeignKey(Routers)
    name = models.CharField(max_length=100)
    ip =  models.CharField(max_length=200, blank=True, null=True)
    hw_id = models.CharField(max_length=17, blank=True, null=True)
    stp_enabled = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=150, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Interfaces(models.Model):
    router_id = models.ForeignKey(Routers)
    name = models.CharField(max_length=50)
    ip = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    hw_id = models.CharField(max_length=17)

    def __unicode__(self):
        return self.name

class Version(models.Model):
    #tutaj prawdopodobnie jedno pole ktore bedzie sluzylo jako backup versji
    pole = models.CharField(max_length=200)

class Arp(models.Model):
    ip = models.IPAddressField()
    hw_type = models.CharField(max_length=50)
    hw_address = models.CharField(max_length=17)
    flags = models.CharField(max_length=20)
    masks = models.CharField(max_length=20)
    iface = models.CharField(max_length=20)

class Router_Date(models.Model):
    #moze sie przyda
    date = models.DateTimeField()

class Hardware(models.Model):
    #podobnie jak wersja
    pole = models.CharField(max_length=200)

class Incoming(models.Model):
    interface = models.CharField(max_length=100)
    action = models.CharField(max_length=100)
    receive = models.CharField(max_length=100)
    dropped = models.CharField(max_length=100)
    overlimit = models.CharField(max_length=100)

from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver

class ConfigFile(models.Model):
    file = models.FileField(upload_to='.')
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return self.title