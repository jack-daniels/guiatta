import paramiko
import os
import re
from guiatta.settings import BASE_DIR
class VyattaRouter(object):

    def __init__(self, ip, username, password):
        self.ip = ip
        self.username = username
        self.password = password
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.cli_path_prefix = '/opt/vyatta/sbin/my_'
        self.session_string = ''

    def connect(self):
        self.ssh.connect(self.ip, username=self.username, password=self.password)
        #initalizing session
        my_string = self.run_vyatta_command('cli_shell_api getSessionEnv $PPID')  #obtain session environment
        #usuwamy z environmentu to czego nie potrzebujemy
        self.session_string = self.__prepare_environment_variables(my_string)
        print self.session_string
        print self.run_vyatta_command('cli_shell_api setupSession')

    def run_command(self, command):
        stdin, stdout, stderr = self.ssh.exec_command(self.session_string + ' ' + command)
        stdin.close()
        error = str(stderr.read())
        if error:
            print 'error'
            return error
        else:
            print 'no error'
            return str(stdout.read())

    def run_vyatta_command(self, command):
        command = self.cli_path_prefix + command
        return self.run_command(command)

    def close_connection(self):
        self.ssh.close()

    def save_config(self, filename='config.cfg'):
        #requires ssh connection
        self.run_command('/opt/vyatta/sbin/vyatta-save-config.pl /home/vyatta/{0}'.format(filename))
        print 'Config saved to {0}'.format(filename)
        self.commit()

    def load_config(self, filename='config.cfg'):
        #requires ssh connection
        self.run_vyatta_command('cli_shell_api loadFile /home/vyatta/{0}'.format(filename))
        print 'Config from {0} loaded'
        self.commit()

    def dump_config(self, filename):
        #does not require 'connect'
        filepath = BASE_DIR
        filepath += "/config_files"
        filepath = os.path.join(filepath, filename)
        remote_path = '/home/vyatta/config.cfg'
        transport = paramiko.Transport((self.ip, 22))
        transport.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.get(remote_path, filepath)
        sftp.close()
        transport.close()

    def put_config(self, filename='config.cfg'):
        #does not require 'connect'
        filepath = BASE_DIR
        filepath += "/user_configs"
        filepath = os.path.join(filepath, filename)
        remote_path = '/home/vyatta/config.cfg'
        transport = paramiko.Transport((self.ip, 22))
        transport.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.put(filepath, remote_path)
        sftp.close()
        transport.close()

    def commit(self):
        return self.run_vyatta_command('commit')

    def set_ethernet_address(self, interface, address):
        """
        address musi byc w postaci: xxx.xxx.xxx.xxx/yy czyli z maska
        """
        return self.run_vyatta_command("set interfaces ethernet {0} address {1}".format(interface, address))

    def __prepare_environment_variables(self, session_env):
        my_string = session_env
        my_string = my_string.replace('>&/dev/null', '') #nie widac outputu i wszystkie commendy daja 'ok'
        my_string = my_string.replace('{', '')
        my_string = my_string.replace('}', '')
        my_string = my_string.replace('|| true', '') #nie wiadomo po co to tam jest :P
        my_string = my_string.replace('declare -x -r', 'export') #declare cos nie dziala, uzywamy normalnego exporta
        my_string = my_string.replace('declare -x', 'export')
        return my_string


def get_interface_address(filename, interface_name):
    return get_parameter_from_eth_config(filename, interface_name, 'address')

def get_interface_mac_address(filename, interface_name):
    return get_parameter_from_eth_config(filename, interface_name, 'hw-id')

def get_interface_description(filename, interface_name):
    return get_parameter_from_eth_config(filename, interface_name, 'description')

def get_bridge_address(filename, bridge_name):
    return get_parameter_from_bridge_config(filename, bridge_name, 'address')

def get_bridge_mac_addres(filename, bridge_name):
    return get_parameter_from_bridge_config(filename, bridge_name, 'mac')

def get_bridge_description(filename, bridge_name):
    return get_parameter_from_bridge_config(filename, bridge_name, 'description')

def get_bridge_stp_enabled(filename, bridge_name):
    return get_parameter_from_bridge_config(filename, bridge_name, 'stp')

def get_parameter_from_eth_config(filename, interface_name, param):
    return get_param_from_config(filename, interface_name, 'ethernet', param)

def get_parameter_from_bridge_config(filename, bridge_name, param):
    return get_param_from_config(filename, bridge_name, 'bridge', param)


def get_param_from_config(filename, name, interface_type, param):
    file = open(filename)
    config = file.read()
    file.close()
    pattern = re.compile(interface_type + ' ' + name + ' {.*?}', re.S)
    found_match = pattern.search(config, re.S)
    if found_match:
        eth_config = config[found_match.start():found_match.end()]
        splitted_config = eth_config.split()
        for number_word, word in enumerate(splitted_config):
            if word == param:
                return splitted_config[number_word+1]
        print "Did not found {0} in {1} config".format(param, name)
    else:
        print "Did not found configuration for bridge {0}".format(name)

def find_interfaces(filename, interface_type):
    file = open(filename)
    interfaces = []
    pattern = re.compile(interface_type + ".*?{")
    for line in file.readlines():
        found_match = pattern.search(line)
        if found_match:
            config_line = line.split()
            interfaces.append(config_line[1])
    file.close()
    return interfaces

def find_ethernet_interfaces(filename):
    interfaces = []
    for interface in find_interfaces(filename, 'ethernet'):
        ip = get_interface_address(filename, interface)
        mac_address = get_interface_mac_address(filename, interface)
        description = get_interface_description(filename, interface)
        dict = {'name': interface, 'ip': ip, 'mac_address': mac_address, 'description': description}
        interfaces.append(dict)
    return interfaces

def find_bridges(filename):
    bridges = []
    for bridge in find_interfaces(filename, 'bridge'):
        ip = get_bridge_address(filename, bridge)
        stp = get_bridge_stp_enabled(filename, bridge)
        mac = get_bridge_mac_addres(filename, bridge)
        description = get_bridge_description(filename, bridge)
        dict = {'name': bridge, 'ip': ip, 'stp_enabled': stp, 'mac': mac, 'description': description}
        bridges.append(dict)
    return bridges


def update_interface(router, interface):
    vyatta = VyattaRouter(router.ip, router.user, router.password)
    vyatta.connect()
    try :
        vyatta.run_vyatta_command('delete interfaces ethernet {0} address'.format(interface.name))
        vyatta.run_vyatta_command('delete interfaces ethernet {0} hw-id'.format(interface.name))
        vyatta.run_vyatta_command('delete interfaces ethernet {0} description'.format(interface.name))
        vyatta.commit()
        vyatta.run_vyatta_command('set interfaces ethernet {0} address {1}'.format(interface.name, interface.ip))
        vyatta.run_vyatta_command('set interfaces ethernet {0} hw-id {1}'.format(interface.name, interface.hw_id))
        vyatta.run_vyatta_command('set interfaces ethernet {0} description {1}'.format(interface.name, interface.description))
        vyatta.commit()
    except Exception as e:
        print "Oh jak pieknie cos nie poszlo!"
        print e.message
    finally:
        vyatta.close_connection()

def delete_inerface(router, interface):
    vyatta = VyattaRouter(router.ip, router.user, router.password)
    vyatta.connect()
    try :
        vyatta.run_vyatta_command('delete interfaces ethernet {0}'.format(interface.name))
        vyatta.commit()
    except Exception as e:
        print "Oh jak pieknie cos nie poszlo!"
        print e.message
    finally:
        vyatta.close_connection()

def update_bridge(router, bridge):
    vyatta = VyattaRouter(router.ip, router.user, router.password)
    vyatta.connect()
    try:
        vyatta.run_vyatta_command('delete interfaces bridge {0} address'.format(bridge.name))
        vyatta.run_vyatta_command('delete interfaces bridge {0} mac'.format(bridge.name))
        vyatta.run_vyatta_command('delete interfaces bridge {0} description'.format(bridge.name))
        vyatta.commit()
        vyatta.run_vyatta_command('set interfaces bridge {0} address {1}'.format(bridge.name, bridge.ip))
        vyatta.run_vyatta_command('set interfaces bridge {0} mac {1}'.format(bridge.name, bridge.mac))
        vyatta.run_vyatta_command('set interfaces bridge {0} stp {1}'.format(bridge.name, bridge.stp_enabled))
        vyatta.run_vyatta_command('set interfaces bridge {0} description {1}'.format(bridge.name, bridge.description))
        vyatta.commit()
    except Exception as e:
        print "Oh jak pieknie cos nie poszlo!"
        print e.message
    finally:
        vyatta.close_connection()

def delete_bridge(router, bridge):
    vyatta = VyattaRouter(router.ip, router.user, router.password)
    vyatta.connect()
    try:
        vyatta.run_vyatta_command('delete interfaces bridge {0}'.format(bridge.name))
        vyatta.commit()
    except Exception as e:
        print "Oh jak pieknie cos nie poszlo!"
        print e.message
    finally:
        vyatta.close_connection()