from django.conf.urls import *
from views import *
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from models import *
from django.views.decorators.cache import never_cache

urlpatterns = patterns('',
    url(r'^$', index),
    url(r'^login/$', 'django.contrib.auth.views.login', {
                'template_name': 'main/login.html'}),
    url(r'^interfaces/$', login_required(never_cache(InterfacesView.as_view() ), ) ),
    url(r'^create_interfaces/$', login_required(CreateInterfacesView.as_view())),
    url(r'^update_interfaces/(?P<pk>\d+)/$', login_required(UpdateInterfacesView.as_view())),
    url(r'^delete_interfaces/(?P<pk>\d+)/$', login_required(DeleteInterfacesView.as_view())),
    url(r'^refresh_configs/$', login_required(refresh_router_configs) ),
    url(r'^bridges/$', login_required(never_cache(BridgeView.as_view() ), ) ),
    url(r'^create_bridges/$', login_required(CreateBridgeView.as_view())),
    url(r'^update_bridges/(?P<pk>\d+)/$', login_required(UpdateBridgeView.as_view())),
    url(r'^delete_bridges/(?P<pk>\d+)/$', login_required(DeleteBridgeView.as_view())),
    url(r'^logout/$', user_logout, name='logout'),
    url(r'^routers/$', login_required(RoutersView.as_view())),
    url(r'^create_routers/$', login_required(CreateRoutersView.as_view())),
    url(r'^update_routers/(?P<pk>\d+)/$', login_required(UpdateRoutersView.as_view())),
    url(r'^delete_routers/(?P<pk>\d+)/$', login_required(DeleteRoutersView.as_view())),
    url(r'^upload_config/$', login_required(uploadConfig)),
    url(r'^configtest/$', testview)
)