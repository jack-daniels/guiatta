from django.contrib import admin

# Register your models here.

import models
admin.site.register(models.Routers)
admin.site.register(models.ConfigFile)

class InterfacesInline(admin.TabularInline):
    model = models.Interfaces

class InterfacesAdmin(admin.ModelAdmin):
    inline = [
        InterfacesInline,
    ]

admin.site.register(models.Interfaces, InterfacesAdmin)
admin.site.register(models.Bridge)