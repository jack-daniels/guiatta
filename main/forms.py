from django import forms
from django.core.exceptions import ValidationError
from models import *
from django.contrib import admin

class RouterForm(forms.ModelForm):
    class Meta:
        model = Routers
        widgets = {
            'password': forms.PasswordInput(),
        }

class InterfacesInline(admin.TabularInline):
    model = Interfaces

class InterfacesForm(forms.ModelForm):
    class Meta:
        model = Interfaces

class BridgeForm(forms.ModelForm):
    class Meta:
        model = Bridge

class UploadFileForm(forms.ModelForm):
    class Meta:
        model = ConfigFile



